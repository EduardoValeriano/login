/**
 * 
 */
package com.example.login.exception;

/**
 * @author Eduardo Valaeriano Sevilla
	5 mar. 2020
	12:21:51
 *
 * 
 */
public class SmartException extends RuntimeException{

		/**
		 * 
		 */
		private static final long serialVersionUID = -822815732695719158L;
		private int errorCode;
		private String errorMessage;
		
		public SmartException(String errorMessage) {
			super(errorMessage);
			this.errorMessage = errorMessage;
		} 
		
		
		public int getErrorCode() {
			return errorCode;
		}
		public void setErrorCode(int errorCode) {
			this.errorCode = errorCode;
		}
		public String getErrorMessage() {
			return errorMessage;
		}
		public void setErrorMessage(String errorMessage) {
			this.errorMessage = errorMessage;
		}
		

}
