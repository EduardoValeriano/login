/**
 * 
 */
package com.example.login.service;

import org.springframework.stereotype.Service;

import com.example.login.DTO.LoginDTO;
import com.example.login.DTO.LoginDTOResponse;

/**
 * @author Eduardo Valaeriano Sevilla
	5 mar. 2020
	12:10:42
 *
 * 
 */
@Service
public interface LoginService {

	public LoginDTOResponse authenticate(LoginDTO loginDTO) throws Exception;
	

}
