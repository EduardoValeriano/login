/**
 * 
 */
package com.example.login.service.impl;

import javax.crypto.Cipher;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.login.DTO.LoginDTO;
import com.example.login.DTO.LoginDTOResponse;
import com.example.login.entity.User;
import com.example.login.exception.SmartException;
import com.example.login.repository.UserRepository;
import com.example.login.service.LoginService;

/**
 * @author Eduardo Valaeriano Sevilla
	5 mar. 2020
	12:18:29
 *
 * 
 */
@Service
public class LoginServiceImpl  implements LoginService{
	@Autowired private UserRepository userRepository;
	private static Cipher rsa;

	@Override
	public LoginDTOResponse authenticate(LoginDTO loginDTO) throws Exception {
		
		if(loginDTO == null ) {
			throw new SmartException("something went wrong");
//			throw new SmartException("the object is empty"); 
		}else if( (loginDTO.getPass().length()<=0) || (loginDTO.getPass().isEmpty()) || (loginDTO.getUser()== null)) {
	//		throw new SmartException("the passs is empty");
			throw new SmartException("something went wrong");
		}else if( (loginDTO.getUser().length()<=0) || (loginDTO.getUser().isEmpty()) || (loginDTO.getUser()==null) ) {
		//	throw new SmartException("the user is empty"); 
			throw new SmartException("something went wrong");
		}		
		
		///------------------------ codigo que desencripta el texto
//		PrivateKey privateKey = loadPrivateKey("/home/smart/Escritorio/claves/privatekey.dat");
//		rsa = Cipher.getInstance("RSA/ECB/PKCS1Padding");
//		rsa.init(Cipher.DECRYPT_MODE, privateKey);
//				
//		byte[] reCipherBytes = Base64.getDecoder().decode(loginDTO.getEncriptado());
//		byte[] bytesDesencriptados2 = rsa.doFinal(reCipherBytes);
//	      String textoDesencripado = new String(bytesDesencriptados2);
//	      System.out.println("aqui tiene que ir el texto desencriptado");
//	      System.out.println(textoDesencripado);
//	      
//	      DatatypeConverter.printBase64Binary(textoDesencripado.getBytes());
//	      System.out.println(" aqui se imprime la contraseña en 64");
//	      System.out.println( DatatypeConverter.printBase64Binary(textoDesencripado.getBytes()) );
	      		
		///------------------------
		
		User usr = new User();	
		usr = userRepository.getPass(loginDTO.getUser());	
		if(usr == null) {
			//throw new SmartException("the query is empty");
			throw new SmartException("something went wrong");
		}else if(!usr.getPassword().equals(loginDTO.getPass())) {
			throw new SmartException("something went wrong");
//			throw new SmartException("el pass no coincide");
		}
		LoginDTOResponse response = new LoginDTOResponse();
		response.setIdResponse(1);
		response.setResponse("exito");
		
		return response;
	}
//	  private PrivateKey loadPrivateKey(String fileName) throws Exception {
//	      FileInputStream fis = new FileInputStream(fileName);
//	      int numBtyes = fis.available();
//	      byte[] bytes = new byte[numBtyes];
//	      fis.read(bytes);
//	      fis.close();
//
//	      KeyFactory keyFactory = KeyFactory.getInstance("RSA");
//	      KeySpec keySpec = new PKCS8EncodedKeySpec(bytes);
//	      PrivateKey keyFromBytes = keyFactory.generatePrivate(keySpec);
//	      return keyFromBytes;
//	   }

}
