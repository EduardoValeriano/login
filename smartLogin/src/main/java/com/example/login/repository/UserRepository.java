/**
 * 
 */
package com.example.login.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.login.entity.User;

/**
 * @author Eduardo Valaeriano Sevilla
	5 mar. 2020
	15:40:31
 *
 * 
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	
	@Query(value=" select * from user " + 
			" where user_name = :usr",nativeQuery=true)
	public User getPass(@Param("usr")String usr);

}
