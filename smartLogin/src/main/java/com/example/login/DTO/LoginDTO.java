/**
 * 
 */
package com.example.login.DTO;

import lombok.Data;

/**
 * @author Eduardo Valaeriano Sevilla
	5 mar. 2020
	12:03:07
 *
 * 
 */
@Data
public class LoginDTO {
	private String user;
	private String pass;
	private String encriptado;

}
