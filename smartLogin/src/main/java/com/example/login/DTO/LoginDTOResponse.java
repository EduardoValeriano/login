/**
 * 
 */
package com.example.login.DTO;

import lombok.Data;

/**
 * @author Eduardo Valaeriano Sevilla
	5 mar. 2020
	12:07:35
 *
 * 
 */
@Data
public class LoginDTOResponse {
	private int idResponse;
	private String response;

}
