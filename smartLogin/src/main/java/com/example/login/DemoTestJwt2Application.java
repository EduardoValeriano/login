package com.example.login;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoTestJwt2Application {

	public static void main(String[] args) {
		SpringApplication.run(DemoTestJwt2Application.class, args);
	}

}
