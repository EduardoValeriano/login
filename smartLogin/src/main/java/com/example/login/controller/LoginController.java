/**
 * 
 */
package com.example.login.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.login.DTO.LoginDTO;
import com.example.login.DTO.LoginDTOResponse;
import com.example.login.service.LoginService;

/**
 * @author Eduardo Valaeriano Sevilla
	5 mar. 2020
	11:58:42
 *
 * 
 */
@RestController
@RequestMapping("/api")
public class LoginController {
	@Autowired private LoginService LoginService;
	
	@PostMapping("/login/version1")
	public ResponseEntity<?> getLogin(
			@RequestBody (required = true) LoginDTO loginDTO ) throws Exception{
		LoginDTOResponse response = new LoginDTOResponse();
		response = LoginService.authenticate(loginDTO); 
		return new ResponseEntity<LoginDTOResponse>(response,HttpStatus.OK);
		
	}
	
	

}
